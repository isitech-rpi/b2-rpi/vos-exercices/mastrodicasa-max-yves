#include "Epee.h"

Epee::Epee() : Arme() {

}

Epee::Epee(int portee, double attaque=4.1, double defense=2.9) : Arme(portee, attaque, defense) {
	
}

Epee::~Epee(){}

double Epee::Utilise(bool attaque) {
	cout << "Ne degaine pas une epee pour tuer un moustique.." << endl;
	return attaque ? puissanceAttaque : puissanceDefense;
}