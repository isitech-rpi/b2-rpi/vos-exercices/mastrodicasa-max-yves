#include <iostream>
#include "Heros.h";
#include "Fourche.h";
#include "Epee.h";


using namespace Personnages;

int main()
{
    // Création auto du Perso1
    Personnage Perso1("Guethenoc");
    Perso1.afficheur();

    //Création dynamique de Perso2
    Personnage* Perso2 = new Personnage("Balcmeg");
    Perso2->afficheur();

    Heros H1("Gollum");
    H1.afficheur();

    Heros* H2 = new Heros("Frodon",10);
    H2->afficheur();

    H1.Attaquer(Perso2);
    H2->Attaquer(&H1);

    Perso2->afficheur();
    H2->afficheur();
    H1.afficheur();

    Fourche arme1();
    Arme* arme2 = new Epee(2);

    H1.CollecterArme(&arme1);
    Perso1.CollecterArme(&arme1);
    H2->CollecterArme(arme2);

    H1.Attaquer(Perso2);
    Perso2->afficheur();

    H2->Attaquer(&H1);
    H1.afficheur();
    H2->afficheur();

    Perso1.Attaquer(&H1);
    H1.afficheur();

    //Destructeur
    delete Perso2;
    delete H2;
    delete arme2;
}


