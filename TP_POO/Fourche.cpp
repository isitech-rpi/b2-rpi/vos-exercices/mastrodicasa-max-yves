#include "Fourche.h"

Fourche::Fourche() : Arme() {

}

Fourche::Fourche(int portee, double attaque=1.4, double defense=2.5) : Arme(portee, attaque, defense) {

}

Fourche::~Fourche() {}

double Fourche::Utilise(bool attaque) {
	cout << "A la fourche, on reconna�t le paysan !" << endl;
	return attaque ? puissanceAttaque : puissanceDefense;
}