#pragma once
#include "Arme.h"
class Epee : public Arme
{
public:
	Epee();
	~Epee();
	double Utilise(bool attaque);
protected:
	Epee(int portee, double attaque=4.1, double defense=2.9);
};

