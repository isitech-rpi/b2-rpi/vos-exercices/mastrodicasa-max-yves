#include <iostream>
#include <iomanip>
#include <random>
#include <time.h>
#undef max
using namespace std;

//1.1
int a = 1;
int b = 5;
double f = 3.14;
char c('C');
char d;

//1.2
int i1 = 1;
int i2 = 3;
int r1;
double d1 = 1;
double d2 = 3;
double r2;

void Exo1() {
    // 1.1 Déclaration, affectation, affichage
    cout << "1.1 Declaration, affectation, affichage" << endl << endl;

    cout << "a = " << dec << a << "\nadress = " << hex << &a << endl << endl;
    cout << "b = " << dec << b << "\nadress = " << hex << &b << endl << endl;
    cout << "f = " << dec << f << "\nadress = " << hex << &f << endl << endl;
    cout << "c = " << dec << c << "\nadress = " << hex << &c << endl << endl;
    cout << "d = " << dec << d << "\nadress = " << hex << &d << endl << endl;

    d = 'D';
    cout << "d = " << dec << d << "\nadress = " << hex << &d << endl << endl;

    decltype(a) temp = a;
    a = b;
    b = temp;
    cout << "a = " << dec << a << "\nadress = " << hex << &a << endl << endl;
    cout << "b = " << dec << b << "\nadress = " << hex << &b << endl << endl;

    temp = a;
    a = f;
    f = temp;
    cout << "a = " << a << "\nadress = " << hex << &a << endl << endl;
    cout << "f = " << f << "\nadress = " << hex << &f << endl << endl;

    a = f;
    cout << "a = " << a << "\nadress = " << hex << &a << endl << endl;
    cout << "f = " << f << "\nadress = " << hex << &f << endl << endl;

    d = 90;
    cout << "d = " << d << "\nnumeric value = " << static_cast<int>(d) << endl << endl;

    d += 255;
    cout << "d = " << d << "\nnumeric value = " << static_cast<int>(d) << endl << endl;

    //1.2 Opération sur les nombres
    r1 = (i1 / i2);
    cout << "r1 = " << r1 << endl << endl;

    r2 = (i1 / i2);
    cout << "r2 = (i1 / i2)\n" << "r2 = " << r2 << endl << endl;

    r2 = (d1 / i2);
    cout << "r2 = (d1 / i2)\n" << "r2 = " << r2 << endl << endl;

    r2 = (i1 / d2);
    cout << "r2 = (i1 / d2)\n" << "r2 = " << r2 << endl << endl;

    r2 = (d1 / d2);
    cout << "r2 = (d1 / d2)\n" << "r2 = " << r2 << endl << endl;

    //1.3 Pointeur

    int aa = 3;
    int bb = 10;
    int* p;

    p = &(bb);
    cout << "p = " << p << "\nadress : " << &(*p) << "\nValeur pointee par p : " << dec << *p << endl << endl;

    p = &aa;
    *p *= 2;
    cout << "Valeur pointee par p x 2 : " << dec << *p << endl << endl;

    p++;
    cout << "p = " << p << "\nValeur pointee par p : " << dec << *p << endl << endl << endl << endl;

};

void ConditionSimple() {
    //2.1
    cout << "2.1 Condition Simple" << endl << endl;

    int heure, minute;

    cout << "Saisissez l'heure : ";
    cin >> heure;
    while (std::cin.fail() || heure < 0 || heure > 23)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (0-23): ";
        cin >> heure;
    }

    cout << "Saisissez les minutes : ";
    cin >> minute;
    while (std::cin.fail() || minute < 0 || minute > 59)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (0-59): ";
        cin >> minute;
    }

    cout << endl << "Vous avez saisi l'heure : " << std::setfill('0') << std::setw(2) << heure << ":" << std::setfill('0') << std::setw(2) << minute << endl;

    minute++;
    if (minute == 60) {
        minute = 0;
        heure++;
        if (heure == 24) {
            heure = 0;
        }
    }
    cout << "Dans un futur proche il sera : " << std::setfill('0') << std::setw(2) << heure << ":" << std::setfill('0') << std::setw(2) << minute;

    cout << endl << endl << endl;
};

void ConditionMultiple() {
    //2.2
    cout << "2.2 Condition Multiples" << endl << endl;

    int age;
    cout << "Il s agit de determiner votre categorie sportive." << endl << endl;
    cout << "Saisissez votre age : ";
    cin >> age;
    while (std::cin.fail() || age < 6 || age > 123)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (age > 123) {
            cout << "Etes vous humain ???" << endl;
        }
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (6-123): ";
        cin >> age;
    }

    if (age < 18) {
        if (age < 16) {
            if (age < 14) {
                if (age < 12) {
                    if (age < 10) {
                        cout << "Vous etes un pre-poussin !" << endl;
                    }
                    else { cout << "Vous etes un poussin !" << endl; }
                }
                else { cout << "Vous etes un benjamin !" << endl; }
            }
            else { cout << "Vous etes un minime !" << endl; }
        }
        else { cout << "Vous etes un cadet !" << endl; }
    }
    else { cout << "Vous etes un senior !" << endl; }

    if (age > 17) { cout << "Vous etes un senior !" << endl; }
    else if (15 < age && age < 18) { cout << "Vous etes un cadet !" << endl; }
    else if (13 < age && age < 16) { cout << "Vous etes un minime !" << endl; }
    else if (11 < age && age < 14) { cout << "Vous etes un benjamin !" << endl; }
    else if (9 < age && age < 12) { cout << "Vous etes un poussin !" << endl; }
    else { cout << "Vous etes un pre-poussin !" << endl; }


    switch (age) {
    case 16: case 17:
        cout << "Vous etes un cadet !" << endl;
        break;
    case 14: case 15:
        cout << "Vous etes un minime !" << endl;
        break;
    case 12: case 13:
        cout << "Vous etes un benjamin !" << endl;
        break;
    case 10: case 11:
        cout << "Vous etes un poussin !" << endl;
        break;
    case 6: case 7: case 8: case 9:
        cout << "Vous etes un pre-poussin !" << endl;
        break;
    default:
        if (age > 17) {
            cout << "Vous etes un senior !" << endl;
        }
    }
};

void Iteration() {
    double note, somme = 0;
    int compteur = 0;

    cout << "Entrez vos notes (tapez -1 pour terminer) : " << endl;
    while (true) {
        cout << "Note " << compteur+1 << " : ";
        cin >> note;

        if (note == -1) {
            break;
        }

        if (note >= 0 && note <= 20) {
            somme += note;
            compteur++;
        }
        else {
            cout << "Note invalide, veuillez entrer une note entre 0 et 20." << endl;
        }
    }

    if (compteur > 0) {
        double moyenne = somme / compteur;
        cout << "La moyenne des notes est : " << moyenne << endl;
    }
    else {
        cout << "Aucune note valide n'a été saisie." << endl;
    }

};

void Exo2() {
    int exo;
    //2 Conditions et itérations
    cout << " 2 Conditions et iterations" << endl;
    cout << "Choisissez l'exercie (1, 2 ou 3) : ";
    cin >> exo;
    while (std::cin.fail() || exo < 1 || exo > 3)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (1, 2, ou 3): ";
        cin >> exo;
    }
    system("cls");
    switch (exo) {
    case 1:
        ConditionSimple();
        break;
    case 2:
        ConditionMultiple();
        break;
    case 3:
        Iteration();
        break;
    }
};

int SaisieNombre(int mini,int maxi) {
    int unEntier;

    cout << "Entrez un entier entre " << mini << " et " << maxi << " : ";
    cin >> unEntier;
    while (std::cin.fail() || unEntier < mini || unEntier > maxi)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (entre " << mini << " et " << maxi << ") : ";
        cin >> unEntier;
    }

    return unEntier;
}

string TirerCarte(int jeu) {
    
    int randomCarte;
    
    if (jeu != 3) {
        int randomCouleur;

        string cartes[13] = {
            "As", "2","3","4","5","6","7","8","9","10","Valet","Dame","Roi"
        };

        string couleur[4] = {
            "Coeur", "Carreau", "Trefle", "Pique"
        };

        randomCarte = rand() % 13;
        if (randomCarte < 6) { randomCarte += jeu; }
        randomCouleur = rand() % 4;

        return cartes[randomCarte] + "_" + couleur[randomCouleur];
    }
    else
    {
        string tarot[22]{
    "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "Excuse"
        };

        randomCarte = rand() % 22;

        return tarot[randomCarte];
    }
}

void ChoixCarte() {
    string carte = "";
    unsigned int compteur = 0;
    int jeu;

    cout << "Tirage de carte !" << endl;
    cout << "Choisissez votre jeu de carte entre 32, 54 cartes ou le tarot ! (1, 2 ou 3)" << endl;
    cin >> jeu;
    while (std::cin.fail() || jeu < 1 || jeu > 3)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (1, 2, ou 3): ";
        cin >> jeu;
    }

    switch (jeu)
    {
    case 1:
        jeu = 6;
        break;
    case 2:
        jeu = 0;
        break;
    case 3:
        break;
    }

    if (jeu != 3) {
        do {
            carte = TirerCarte(jeu);
            compteur++;
            cout << carte << endl;
        } while (carte != "7_Coeur" && carte != "7_Carreau");

        cout << "J'ai tirer " << compteur << " cartes pour avoir un " << carte << endl;
    }
    else {
        do {
            carte = TirerCarte(jeu);
            compteur++;
            cout << carte << endl;
        } while (carte != "Excuse");

        cout << "J'ai tirer " << compteur << " cartes pour avoir l'" << carte << endl;
    };
};


void Permute() {

}

void Exo3() {
    int exo;
    //3 Fonctions
    cout << " 3 Fonctions" << endl;
    cout << "Choisissez l'exercie (1, 2 ou 3) : ";
    cin >> exo;
    while (std::cin.fail() || exo < 1 || exo > 3)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (1, 2, ou 3): ";
        cin >> exo;
    }
    system("cls");
    srand(time(NULL));
    switch (exo) {
    case 1:
        SaisieNombre(0,999);
        break;
    case 2:
        ChoixCarte();
        break;
    case 3:
        Permute();
        break;
    }
}

int main()
{
    int partie;
    while (true)
    {
        cout << "Choisissez la partie du TP (1, 2 ou 3) : "; 
        cin >> partie;
        while (std::cin.fail() || partie < 1 || partie > 3)
        {
            cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (1, 2 ou 3): ";
            cin >> partie;
        }
        system("cls");
        switch (partie) {
        case 1:
            Exo1();
            cout << endl << endl;
            break;
        case 2:
            Exo2();
            cout << endl << endl;
            break;
        case 3:
            Exo3();
            cout << endl << endl;
            break;
        }
    }
}
