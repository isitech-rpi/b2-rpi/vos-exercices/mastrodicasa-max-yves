#pragma once
#include "Arme.h"
class Fourche : public Arme
{
public:
	Fourche();
	~Fourche();
	double Utilise(bool attaque);
protected:
	Fourche(int portee ,double attaque=1.4, double defense=2.5);
};

