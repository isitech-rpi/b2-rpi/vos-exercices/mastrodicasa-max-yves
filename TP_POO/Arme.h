#pragma once
#include <iostream>

using namespace std;

	class Arme
	{ protected:
		double puissanceAttaque =0.0;
		double puissanceDefense=0.0;
		int portee=0;

	public:
		Arme();

		Arme(double attaque, double defense, int portee);

		~Arme();

		double Utilise(bool attaque);
	};

