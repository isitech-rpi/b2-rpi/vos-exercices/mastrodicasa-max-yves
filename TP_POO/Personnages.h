#pragma once
#include <string>
#include "Arme.h"

namespace Personnages
{
	class Personnage{
	private :
		std::string nom;

	protected :
		double vie;
		int niveau;
		bool Defendre(Personnage* attaquant);
		Arme* armeCourante;

	public :
		
		Personnage(std::string nom);

		~Personnage();

		bool Attaquer(Personnage* ennemi);

		bool EstVivant() const;

		void CollecterArme(Arme* arme);

		void afficheur() const;
	};
}

