#include "Cours.h"
#include <string>
#include <iostream>
#include <Windows.h>
using namespace std;

    
    Cours::Cours() {
        matiere = "C++";
        promo = "B2 RPI";
        cout << "Constructeur par defaut. Adresse : " << this << "        Matiere : " << matiere << " - Promo : " << promo << endl;
    }

    Cours::Cours(string matiere, string promo) {
        this->matiere = matiere;
        this->promo = promo;
        cout << "Constructeur d'objet. Adresse : " << this << "        Matiere : " << matiere << " - Promo : " << promo << endl;
    }

    Cours::~Cours() {
        cout << "Destruction de l'objet : " << this << endl;
    };

    void Cours::setPromo(string promo) {
        this->promo = promo;
        cout << "setPromo : nouvelle valeur : " << promo << endl;
    }

    void Cours::Enseigner() const {
        cout << "Cours " << matiere << " avec la classe " << promo << endl;
    }


