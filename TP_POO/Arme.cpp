#include "Arme.h"

	Arme::Arme()
	{
		cout << "Arme vide cree" << endl;
	}

	Arme::Arme(double attaque, double defense, int portee) {
		this->puissanceAttaque = attaque;
		this->puissanceDefense = defense;
		this->portee = portee;
	}

	Arme::~Arme() {
		cout << "Arme detruite" << endl;
	}

	double Arme::Utilise(bool attaque) {
		return attaque ? puissanceAttaque : puissanceDefense;
	}