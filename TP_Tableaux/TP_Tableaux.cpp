#include <iostream>
#include <windows.h>
#include <algorithm>

#undef max
using namespace std;


void afficheMenu()
{
	system("cls");	//  Efface l'écran

	cout << endl;
	cout << ">>>>>>>>>><<<<<<<<<<<" << endl;
	cout << ">>>>>   MENU   <<<<<<" << endl;
	cout << ">>>>>>>>>><<<<<<<<<<<" << endl << endl;
	cout << "1) Remplir" << endl;
	cout << "2) Copier" << endl;
	cout << "3) Trier" << endl;
	cout << "4) Quitter" << endl << endl << endl;
}

void pause() {
	system("pause");
}

int choixPartie(int partie) {
	cin >> partie;
	while (std::cin.fail() || partie < 1 || partie > 3)
	{
		cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (1, 2 ou 3): ";
		cin >> partie;
	}
	system("cls");
	return partie;
}

void remplirTab(int* tab, int lngTab) {
	for (int i = 0; i < lngTab; i++) {
		cout << "Element " << i + 1 << " du tableau : ";
		cin >> tab[i];

		while (std::cin.fail()) {
			cout << "Saisie incorrect, veuillez saisir un nombre correct : " << endl;
			cin >> tab[i];
		}
	}

}

int* copie(const int* src, int lngTab) {
	int* newTab = new int[lngTab];
	for (int i = 0; i < lngTab; i++) {
		newTab[i] = src[i];
	}
	return newTab;
}

void sortTab(int* tab1, int lngTab, bool ordreCroissant = true) {
	for (int i = 0; i < lngTab - 1; i++) {
		int indExt = i;
		for (int j = i + 1; j < lngTab; j++) {
			if (ordreCroissant ? tab1[j] < tab1[indExt] : tab1[j] > tab1[indExt]) {
				indExt = j;
			}
		}
		if (indExt != i) {
			int temp = tab1[i];
			tab1[i] = tab1[indExt];
			tab1[indExt] = temp;
		}
	}
}

int main()
{
    std::cout << "1. Tableau\n" << endl ;
	bool quit = false;
	int choix = 0;
	const int lngTab = 5;
	int tab1[lngTab];
	int* tabCopie;
	bool sorting = false;
	SetConsoleOutputCP(CP_UTF8);

	//  Initialisation pour le générateur de nombres aléatoires
	srand((unsigned int)std::time(0));

	do
	{
		//  Affiche le menu et lit le choix de l'utilisateur
		afficheMenu();
		choix = choixPartie(0);

		switch (choix)	//  Réalise l'action choisie par l'utilisateur
		{
		case 1:

			cout << "\nOn remplie le tableau d'entier de taille " << lngTab << endl;
			remplirTab(tab1,lngTab);

			cout << endl << endl << "Votre tableau : ";
			for (int i = 0; i < lngTab; i++) {
				std::cout << tab1[i] << ", ";
			}
			cout << endl;
			pause();
			break;
		case 2:
			tabCopie = copie(tab1, lngTab);
			cout << endl << endl << "Tableau copié : ";
			for (int i = 0; i < lngTab; i++) {
				std::cout << tabCopie[i] << ", ";
			}
			cout << endl;

			pause();
			break;
		case 3:

			cout << "Voulez vous triez votre tableau par order croissant (1), ou décroissant (2)" << endl;
			cin >> choix;
			if (choix == 1) {
				sorting = true;
			}
			else { sorting = false; }
			sortTab(tab1, lngTab, sorting);
			pause();
			break;
		case 4:
			quit = true;
			break;
		}
	} while (!quit);

}
