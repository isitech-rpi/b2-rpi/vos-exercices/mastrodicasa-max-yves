#include "Personnages.h"
#include <iostream>

using namespace std;

namespace Personnages {
	Personnage::Personnage(string nom) : nom(nom), vie(10.0), niveau(1) {
		this->armeCourante = nullptr;
		cout << "Constructeur : " << this << endl;
	}

	Personnage::~Personnage() {
		cout << "Bonjour, je suis le Demolisseur ! " << this << endl;
	}

	void Personnage::afficheur()const {
		cout << nom << " - stat : PV[" << vie << "] / lvl[" << niveau << "]\n";
	}

	bool Personnage::Attaquer(Personnage* ennemi) {
		if (!ennemi->EstVivant()) {
			return false;
		}
		double degatArme = 0.0;
		if (armeCourante != nullptr) {
			degatArme = armeCourante->Utilise(true);
		}
		double degats = (vie+degatArme) * niveau / 20.0;
		
		ennemi->vie -= degats;
			if (!ennemi->EstVivant()) {
				ennemi->vie = 0;
			}
			cout << nom << " attaque " << ennemi->nom << " : il inflige " << degats << " degats" <<endl;
		return ennemi->Defendre(this);
	}

	bool Personnage::EstVivant() const {
		if (vie > 0) {
			return true;
		}
		else { return false; }
	}

	bool Personnage::Defendre(Personnage* attaquant) {
		if (!EstVivant()) {
			return false;
		}
		double degatArme = 0.0;
		if (armeCourante != nullptr) {
			degatArme = armeCourante->Utilise(false);
		}
		double degats = (attaquant->vie+degatArme)*attaquant->niveau / 20.0;
		attaquant->vie -= degats;
		if (vie < 0) {
			vie = 0;
		}
		cout << nom << " se defend et inflige " << degats << " degats" << endl;
		return EstVivant();
	}

	void Personnage::CollecterArme(Arme* arme) {
		this->armeCourante = arme;
		cout << nom << " a pris une arme en main !" << endl;
	}
}


